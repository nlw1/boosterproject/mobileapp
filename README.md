# Projeto Mobile
- Nossa versão mobile da aplicação Ecoleta.

# Tecnologias utilizadas

## ReactNative
- Um projeto desenvolvido pelo Facebook, baseado no React.js.
- Feito para possibilitar a criação de aplicações nativas para Android e IOS.
- É importante deixar claro que o código javaScript não é convertido em código nativo. Na verdade ele será transpilado.
- O dispositivo móvel consegue entender o JS e a interface gerada é toda nativa.
- Nos permite utilizar ECMAScript 6, CSS Flexbox, JSX, diversos pacotes do NPM e muito mais.

## ReactNative X ReactJS
### Semelhanças
- Escrevemos o código do mesmo jeito. Uma função que retorna um HTML.
    - XML dentro do JavaScript - JSX.
- Os mesmos conceitos do ReactJS serão usados aqui.
    - Estado;
    - Imutabilidade;
    - Componentização;
    - Acesso a rotas.

### Diferenças
- No ReactNative não temos as tags HTML.
- Vamos usar as próprias tags do ReactNative.
- Toda estilização no mobile tem de ser feito na mão.
    - Classe StyleSheet
    - Todo componente tem uma propriedade styles que pode receber um StyleSheet.
    - A estilizaçao é feita com javascript e não com um arquivo css.
    - Contudo as propriedades (nomenclaturas) são as mesmas.
        - Dessa forma conseguimos reaproveitar o conhecimento de css Web para desenvolvimento Mobile.
        - Isso é possível devido à engine Yoga criada pelo Facebook que converte essa estilização css para estilização nativa mobile.
        - Link Yoga: https://yogalayout.com/
    - Onde tínhamos o hífen no css padrão, no css ReactNative, colocamos letra maiúscula na próxima palavra.
        - Ex: background-color --> backgroundColor.
    - Todos os elementos são display flex.
    - No native não temos herança nem cascata de estilos.

## Expo
- Quando desenvolvemos para dispositivos móveis, temos de ter todo um ambiente configurado em nossa máquina.
- Android
    - SDK de desenvolvimento
- IOS
    - Xcode para obter a SDK do IOS
- Sendo assim toda vez que precisarmos utilizar um recurso nativo de câmara, de sensores do celular de bateria, precisaremos instalar uma biblioteca específica.
- Com o EXPO, instalamos um aplicativo no celular chamado Expo e, dentro dele, tudo o que precisamos para desenvolver em React Native já está instalado.
- Com isso, não precisamos nos preocupar em gerar o aplicativo para IOS e ANDROID.
- o Expo vai facilitar muito nosso desenvolvimento da aplicação.

# O que termos na nossa interface mobile
- A interface para o usuário poder acessar os dados necessários da api REST.

# Inicando o projeto
## expo-cli
- Instalar globalmente a biblioteca expo-cli, pois ela irá nos auxiliar na criação do projeto.
- executar npm start para iniciar o bundler.

## bundler
- Um código javascript mimificado para ser colocado no celular.

## Instalação do expo no celular
- Entrar na appstore do seu sistema e baixar.
- Scanner do QRcode.
- Altere o arquivo App.tsx para testar.

### Issues para o expo em caso de algum problema
- Link: https://github.com/Rocketseat/expo-common-issues

# Importando imagens em aplicações mobile
- É interessante que você tenha a mesma imagem sempre em 3 tamanhos diferentes.
- Pois haverão celulares com densidades de pixels maiores do que outros.

# Importando google fonts através do expo
- Link: https://github.com/expo/google-fonts

# Navegação entre rotas com o React Navigation
- Link: https://reactnavigation.org/docs/getting-started

# Enviando e-mails com o expo mail-composer
- Link: https://docs.expo.io/versions/latest/sdk/mail-composer/

# TODO criar a busca na api do IBGE
- Utilizar: https://github.com/lawnstarter/react-native-picker-select