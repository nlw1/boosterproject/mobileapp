import React from 'react'
import RNPickerSelect, { Item } from 'react-native-picker-select';

interface Props {    
    lista: String[];
}

const Dropdown: React.FC<Props> = ({ lista }) => {
    const teste=[
        { label: 'Football', value: 'football' },
        { label: 'Baseball', value: 'baseball' },
        { label: 'Hockey', value: 'hockey' },
    ];

    const itemsProps = lista.map(item => ({ label: item, value: item }));

    return (
        <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={itemsProps}
        />
    );
};

export default Dropdown;