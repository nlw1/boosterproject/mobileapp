import React, { useState, useEffect, ChangeEvent } from 'react';
import { Feather as Icon } from '@expo/vector-icons';
import { View, ImageBackground, Image, StyleSheet, Text, TextInput, KeyboardAvoidingView, Platform } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import Dropdown from '../../components/Select';
import RNPickerSelect, { Item } from 'react-native-picker-select';

interface IBGEUfResponse {    
  sigla: string
}

interface IBGECityResponse {    
  nome: string
}

const Home = () => {
    const navigation = useNavigation();
    const [uf, setUf] = useState('');
    const [city, setCity] = useState('');

    const [ufs, setUfs] = useState<{
      label: string;
      value: string;
      }[]>([]);
    const [cities, setCities] = useState<{
      label: string;
      value: string;
      }[]>([]);
    const [selectedUf, setSelectedUf] = useState('0');
    const [selectedCity, setSelectedCity] = useState('0');

    useEffect(() => {
      axios.get<IBGEUfResponse[]>('https://servicodados.ibge.gov.br/api/v1/localidades/estados').then(response => {
          const ufInitials = response.data.map(uf => uf.sigla);
          console.log(ufInitials);
          const itemsUfs = ufInitials.map(uf => ({ label: uf, value: uf }));
          console.log(itemsUfs);
          setUfs(itemsUfs);            
      });
    }, []);

    useEffect(() => {
        // carregar as cidades sempre que a cidade mudar
        axios.get<IBGECityResponse[]>(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedUf}/municipios`).then(response => {
            const ufCities = response.data.map(city => city.nome);
            console.log(ufCities);
            const itemsCities = ufCities.map(city => ({ label: city, value: city }));
            setCities(itemsCities);
        });
    }, [selectedUf]);

    function handleNavigateToPoints(){
        navigation.navigate('Points', {
          uf,
          city
        });
    }

    /**
     * Função para armazenar a UF escolhida em tela no estado de Ufs.
     * @param event 
     */
    function handleSelectUf(event: ChangeEvent<HTMLSelectElement>){
      setUf(String(event));
      setSelectedUf(String(event));
  }

  /**
   * Função para armazenar a cidade escolhida em tela no estado de cidades.
   * @param event 
   */
  function handleSelectCity(event: ChangeEvent<HTMLSelectElement>){
      setCity(String(event));
      setSelectedCity(String(event));
  }

    return (
      <KeyboardAvoidingView style={{flex: 1}} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
        <ImageBackground 
            source={require('../../../assets/home-background.png')} 
            style={styles.container}
            imageStyle={{width: 274, height: 368}}>            
            <View style={styles.main}>
                <Image source={require('../../../assets/logo.png')}/>
                <View>
                  <Text style={styles.title}>Seu marketplace de coleta de resíduos</Text>
                  <Text style={styles.description}>Ajudamos pessoas a econtrarem pontos de coleta de forma eficiente.</Text>
                </View>
            </View>

            <View style={styles.footer}>
                <RNPickerSelect
                  placeholder={{
                    label: 'Selecione a UF',
                    value: null,
                    color: '#808080'
                  }}
                  style={{
                    ...pickerSelectStyles, viewContainer: {borderRadius: 10}
                  }}
                  onValueChange={handleSelectUf}
                  items={ufs}
                />

                <RNPickerSelect
                  placeholder={{
                    label: 'Selecione a cidade',
                    value: null,
                    color: '#808080'
                  }}
                  style={{
                    ...pickerSelectStyles,
                    chevronActive: {
                      borderRadius: 10,
                    }                    
                  }}
                  onValueChange={handleSelectCity}
                  items={cities}
                />

                <RectButton style={styles.button} onPress={handleNavigateToPoints}>
                    <View style={styles.buttonIcon}>
                        <Text>
                            <Icon name="arrow-right" color="#FFF" size={24} />
                        </Text>
                    </View>
                    <Text style={styles.buttonText}>Entrar</Text>
                </RectButton>
            </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    height: 70,
    backgroundColor: '#FFF',
    borderRadius: 10,
    marginBottom: 8,
    paddingHorizontal: 24,
    fontSize: 16,
    color: '#000000',
    paddingVertical: 8,
  },
  inputAndroid: {
    height: 70,
    backgroundColor: '#FFF',    
    marginBottom: 8,
    paddingHorizontal: 24,
    fontSize: 16,
    color: '#000000',
    paddingVertical: 8,
  },
});

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 32      
    },
  
    main: {
      flex: 1,
      justifyContent: 'center',
    },
  
    title: {
      color: '#322153',
      fontSize: 32,
      fontFamily: 'Ubuntu_700Bold',
      maxWidth: 260,
      marginTop: 64,
    },
  
    description: {
      color: '#6C6C80',
      fontSize: 16,
      marginTop: 16,
      fontFamily: 'Roboto_400Regular',
      maxWidth: 260,
      lineHeight: 24,
    },
  
    footer: {},
  
    select: {
      height: 60,
      backgroundColor: '#FFF',
      borderRadius: 10,
      marginBottom: 8,
      paddingHorizontal: 24,
      fontSize: 16,
    },
  
    input: {
      height: 60,
      backgroundColor: '#FFF',
      borderRadius: 10,
      marginBottom: 8,
      paddingHorizontal: 24,
      fontSize: 16,
    },
  
    button: {
      backgroundColor: '#34CB79',
      height: 60,
      flexDirection: 'row',
      borderRadius: 10,
      overflow: 'hidden',
      alignItems: 'center',
      marginTop: 8,
    },
  
    buttonIcon: {
      height: 60,
      width: 60,
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      justifyContent: 'center',
      alignItems: 'center'
    },
  
    buttonText: {
      flex: 1,
      justifyContent: 'center',
      textAlign: 'center',
      color: '#FFF',
      fontFamily: 'Roboto_500Medium',
      fontSize: 16,
    }
  });

export default Home;